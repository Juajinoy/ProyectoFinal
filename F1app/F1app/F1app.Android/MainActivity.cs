﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;

namespace F1app.Droid
{
    [Activity(Label = "F1app", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            //INICIO CONFIGURACION BD
            string name_file_db = "user.sqlite";
            string path_android =System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path_db = Path.Combine(path_android, name_file_db);
            //FIN CONFIGURACION BD
            LoadApplication(new App(path_db));
        }
    }
}

