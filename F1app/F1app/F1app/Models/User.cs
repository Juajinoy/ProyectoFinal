﻿using System;
using System.Collections.Generic;
using System.Text;

namespace F1app.Models
{
    class User
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime Fecha_nacimiento { get; set; }
        public string Password { get; set; }
    }
}
