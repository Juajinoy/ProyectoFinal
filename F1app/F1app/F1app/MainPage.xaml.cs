﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace F1app
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        // INICIO BOTON PARA PASAR AL REGISTRO
        private void pasarRegistro(object sender, EventArgs e){
            ((NavigationPage)this.Parent).PushAsync(new Registro());
        }
        // FIN BOTON PARA PASAR AL REGISTRO

        // INICIO BOTON PARA PASAR AL HOME
        private void pasarHome(object sender, EventArgs e)
        {
            ((NavigationPage)this.Parent).PushAsync(new Home());
        }
        // FIN BOTON PARA PASAR AL HOME
    }
}
