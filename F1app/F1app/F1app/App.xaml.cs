﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace F1app
{
	public partial class App : Application
	{
        public static string urlBd;
		public App ()
		{
			InitializeComponent();

			//MainPage = new F1app.MainPage();
            MainPage = new NavigationPage(new MainPage());
        }

        //INICIO DE CONFIGURACION APP BD
        public App(string url)
        {
            InitializeComponent();
            urlBd = url;

            MainPage = new NavigationPage(new MainPage());
        }
        //FIN DE CONFIGURACION APP BD

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
